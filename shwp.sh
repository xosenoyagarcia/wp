#!/bin/bash
#Author: Xosé Brais Noya
#TESTEADO EN DEBIAN 8.5 SERVER CON LA SIGUIENTE CONFIGURACIÓN:
#En virtual box la configuracion de red utilizada es:
#adaptador 1 NAT
#adaptador 2 Host only adapter vboxnet0 (Se puede añadir en settings > Network > Host Only Adapter)
# Requerir uso de root para ejecutar el script
if [ "`id -u`" != 0 ] ; 
	then
		echo -e "\n"
	    clear
		echo "Es necesario ser Root para ejecutar el script"
	    echo "Pulsa una tecla para continuar..."
		read a
	    clear
	    exit 1
fi
echo "-------------------------------------"
echo "		AUTHOR: Xosé 		   "
echo "-------------------------------------"
echo "-------------------------------------"
echo " Script de instalación de Wordpress  "
echo "-------------------------------------"
echo "-------------------------------------"
echo "Este script configura el equipo para poder utiliar wordpress."
echo "Eres libre de modificarlo según tus necesidades."
echo "Una vez ejecutado el script el sistema se reiniciará, después del reinicio para proceder a la configuración del wordpress:"
echo "              tuiplocal/wp-admin           "
echo "              192.168.56.101/wp-admin      "
echo "En root/ se encuentra un documento informativo sobre los usuarios y contraseñas utiliadas durante la instalación"
echo "Pulsa una tecla para continuar..."
read a
#OPCIONAL- Creacion de ficheros de info
#Puedes usar esta info a la hora de crear las cuentas --PREFIERO USAR ECHOS Y NO EOF
cd
echo "Una vez instalados los servicios damos permisos a www/html y descargamos alli los archivos de wordpress." > infoworpress.txt
echo "Una vez descargado en wordpress podemos acceder de la siguiente manera:" >> infoworpress.txt
echo "ip/wp-login.php" >> infoworpress.txt
echo "ip/phpmyadmin" >> infoworpress.txt
echo "USUARIOS:" >> infoworpress.txt
echo "base de datos: wp" >> infoworpress.txt
echo "phpmyadmin: root" >> infoworpress.txt
echo "sistema: root / ladmin" >> infoworpress.txt
echo "wordpress: admin" >> infoworpress.txt
echo "passwords para los servicios: abc123." >> infoworpress.txt
#CONFIGURACION DE RED:
#copia de seguridad
cp /etc/network/interfaces /etc/network/interfaces.BACKUP
#configuracion de la red - eth0 ya está configurada
echo "#eth1 config" >> /etc/network/interfaces
echo "auto eth1" >> /etc/network/interfaces
echo "iface eth1 inet dhcp" >> /etc/network/interfaces
#reiniciar red
service networking restart

#REPOSITORIOS LOCALES IESSANCLEMENTE
cd /etc/apt/apt.conf.d/
#wget http://mirror/02proxy
wget http://10.0.5.7/02proxy
apt-get update
#WORDPRESS
#instalacion de servicios para wordpress
#apt-get install debconf-utils -y
apt-get install -y debconf-utils
#apt-get install apache2 php5 mysql-server phpmyadmin openssh-server vim git -y --force-yes
apt-get install apache2 php5 -y --force-yes

#para mysql-server autoconfig con debconf
export DEBIAN_FRONTEND=noninteractive
echo "mysql-server-5.5 mysql-server/root_password password abc123." | debconf-set-selections
echo "mysql-server-5.5 mysql-server/root_password_again password abc123." | debconf-set-selections
apt-get install mysql-server-5.5 -y --force-yes
#SI FALLA COMENTAR LAS LINEAS desde export hasta aquí y descomentar las 3 de abajo
#export DEBIAN_FRONTEND=noninteractive
#sudo -E apt-get -q -y install mysql-server
#mysqladmin -u root password abc123.

apt-get install phpmyadmin openssh-server vim git -y --force-yes
#crear base de datos para wordpress la contraseña sin espacio pegada a -p
mysql -u root -pabc123. -e "create database wp"
mysql -u root -pabc123. -e "CREATE USER 'wp'@'localhost' IDENTIFIED BY 'abc123.'"
mysql -u root -pabc123. -e "GRANT ALL PRIVILEGES ON * . * TO 'wp'@'localhost'"
mysql -u root -pabc123. -e "FLUSH PRIVILEGES"
#bajar wordpress
cd /var/www/html/
wget https://wordpress.org/lastest.tar.gz
tar zxf lastest.tar.gz
cd wordpress/
mv * /var/www/html/
rm -rf wordpress/
#permisos modo cutre, la realidad es que sirve un 765 para padre y un 665 para los archivos.
chmod 777 /var/www/html
chown -R www-data /var/www/html
#permitir conectarse usando root --NO RECOMENDADO EN SISTEMA DE PRODUCCION, en produccion generar claves
cd /etc/ssh/
mv sshd_config sshd_config.BACK
sed 's/^PermitRootLogin without-password/#PermitRootLogin without-password/g' sshd_config.BACK > sshd_config
echo "PermitRootLogin yes" >> sshd_config
service ssh restart
#aumentar el tamaño de subida de ficheros para wordpress editar php.ini
cd /etc/php5/apache2/
mv php.ini php.ini.BACK
sed 's/^upload_max_filesize = 2M/upload_max_filesize = 50M/g' php.ini.BACK > php.ini.TEMP
sed 's/^post_max_size = 8M/post_max_size = 50M/g' php.ini.TEMP > php.ini
rm -f php.ini.TEMP
#activar modulos de apache
cd /etc/apache2/sites-enabled/
echo "<Directory /var/www/html>" >> 000-default.conf
echo "AllowOverride All" >> 000-default.conf
echo "</Directory>" >> 000-default.conf
cd /var/www/html
a2enmod rewrite
service apache2 restart 
#REINICIAR ES PARA LOS COBARDES :)
reboot